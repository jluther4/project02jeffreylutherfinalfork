﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_FallowPlayer : MonoBehaviour {
    public GameObject player;
    private Vector3 offset;
	// Use this for initialization
	void Start () {
        //set offset to the diffrence in the camera position to the player position
        offset = gameObject.transform.position - player.transform.position; 
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        //set positon to players position + offset
        gameObject.transform.position = player.transform.position + offset;
	}
}
