﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Iventory : MonoBehaviour {

    public GameObject weaponTwo;
    public GameObject weaponThree;
    public GameObject weaponFour;

    public GameObject heldWeaponOne;
    public GameObject heldWeaponTwo;
    public GameObject heldWeaponThree;
    public GameObject heldWeaponFour;

    public Image Weapon1UI;
    public Image Weapon2UI;
    public Image Weapon3UI;
    public Image Weapon4UI;

    public Image Weapon1UIActive;
    public Image Weapon2UIActive;
    public Image Weapon3UIActive;
    public Image Weapon4UIActive;



    public List<GameObject> weapons = new List<GameObject>();
	// Use this for initialization
	void Start () {
     Weapon2UI.enabled = false;
     Weapon3UI.enabled = false;
     Weapon4UI.enabled = false;

    Weapon2UIActive.enabled = false;
    Weapon3UIActive.enabled = false;
    Weapon4UIActive.enabled = false;

}

// Update is called once per frame
void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1) && heldWeaponOne.activeInHierarchy == false)
        {
            heldWeaponTwo.SetActive(false);
            heldWeaponThree.SetActive(false);
            heldWeaponFour.SetActive(false);
            heldWeaponOne.SetActive(true);
            Weapon1UIActive.enabled = true;
            Weapon2UIActive.enabled = false;
            Weapon3UIActive.enabled = false;
            Weapon4UIActive.enabled = false;

        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && weaponTwo.activeInHierarchy == false)
        {
            heldWeaponThree.SetActive(false);
            heldWeaponFour.SetActive(false);
            heldWeaponOne.SetActive(false);
            heldWeaponTwo.SetActive(true);
            Weapon1UIActive.enabled = false;
            Weapon2UIActive.enabled = true;
            Weapon3UIActive.enabled = false;
            Weapon4UIActive.enabled = false;

        }

        else if (Input.GetKeyDown(KeyCode.Alpha3) && weaponThree.activeInHierarchy == false)
        {
            heldWeaponTwo.SetActive(false);
            heldWeaponFour.SetActive(false);
            heldWeaponOne.SetActive(false);
            heldWeaponThree.SetActive(true);
            Weapon1UIActive.enabled = false;
            Weapon2UIActive.enabled = false;
            Weapon3UIActive.enabled = true;
            Weapon4UIActive.enabled = false;
        }

        else if (Input.GetKeyDown(KeyCode.Alpha4) && weaponFour.activeInHierarchy == false)
        {
            heldWeaponTwo.SetActive(false);
            heldWeaponThree.SetActive(false);
            heldWeaponOne.SetActive(false);
            heldWeaponFour.SetActive(true);
            Weapon1UIActive.enabled = false;
            Weapon2UIActive.enabled = false;
            Weapon3UIActive.enabled = false;
            Weapon4UIActive.enabled = true;
        }
    }

    public void OnTriggerEnter (Collider other)
    {

        if (other.gameObject == weaponTwo)
        {
            Weapon2UI.enabled = true;
            weaponTwo.SetActive(false);
            
        }

        if (other.gameObject == weaponThree)
        {
            Weapon3UI.enabled = true;
            weaponThree.SetActive(false);

        }

        if (other.gameObject == weaponFour)
        {
            Weapon4UI.enabled = true;
            weaponFour.SetActive(false);

        }

       
    }
}
